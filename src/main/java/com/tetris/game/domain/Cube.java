package com.tetris.game.domain;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Cube extends JLabel {

    private int line;

    public Cube() {
        setBounds(240, 0, 40, 40);
        line=6;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setColor(Color.black);
        graphics2D.setStroke(new BasicStroke(10));

        Rectangle2D rectangle2D = new Rectangle2D.Double(2, 2, getWidth()-4, getHeight()-4);
        graphics2D.draw(rectangle2D);
        graphics2D.setColor(Color.orange);
        graphics2D.fill(rectangle2D);
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }
}
