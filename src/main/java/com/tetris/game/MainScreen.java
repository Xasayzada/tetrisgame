package com.tetris.game;

import javax.swing.*;
import java.awt.*;

public class MainScreen extends JFrame {

    private static MainScreen mainScreen;
    private GameScreen gameScreen;

    private MainScreen()  {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setCoordinats(486, 628);
        setResizable(false);
        gameScreen= new GameScreen();
        gameScreen.setSpeed(300);
        gameScreen.start();
        add(gameScreen);
    }

    public static MainScreen newInstance(){
        if (mainScreen==null)
            return new MainScreen();
        return mainScreen;
    }

    public void setCoordinats(int width, int height){
        Dimension  dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int xCoord=(dimension.width-width)/2;
        int yCoord=(dimension.height-height)/2;
        setBounds(xCoord, yCoord, width, height);
    }

}
