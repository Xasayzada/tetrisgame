package com.tetris.game;
import com.tetris.game.domain.Cube;
import com.tetris.game.service.*;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class GameScreen extends JLabel {

    private List<Cube> oldCubeList;
    private List<Cube> cubeList;
    private boolean[][] matrix;
    private ControlPanel controlPanel;
    private Timer timer;
    private ShapeFactory shapeFactory;
    private int speed;
    private int score;
    private int bestScore;
    private Utility utility;
    private MatrixService matrixService;
    private TurnOperationService turnOperationService;

    public GameScreen() {
        matrixService = new MatrixServiceImpl();
        turnOperationService = new TurnOperationServiceImpl();
        utility = new Utility();
        score = 0;
        speed = 250;
        matrix = new boolean[12][16];
        cubeList = new ArrayList<>();
        oldCubeList = new ArrayList<>();
        shapeFactory = new ShapeFactory();
        controlPanel = new ControlPanel();
    }

    {
        setFocusable(true);
    }

    public void start()  {
        for (int i = 0; i < 12; i++) {
            matrix[i][15] = true;
        }
        addKeyListener(controlPanel);
        oldCubeList = shapeFactory.getCubeList(new Random().nextInt(9));
        cubeList = oldCubeList;
        matrixService.getMatrixCoordFromList(oldCubeList, matrix);
        addCubeListToScreen(oldCubeList);
        controlPanel.setCubeList(cubeList);
        controlPanel.setMatrix(matrix);
        try {
            bestScore = utility.getBestScore();
        } catch (IOException e) {
          bestScore=0;
        }
        timer = new Timer(speed, new TimerC());
        timer.start();
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setColor(Color.green);
        graphics2D.setStroke(new BasicStroke(1));
        Rectangle2D rectangle2D = new Rectangle2D.Double(2, 2, getWidth() - 4, getHeight() - 4);
        graphics2D.draw(rectangle2D);
        showScore(graphics2D);
    }

    private void showScore(Graphics2D g2) {
        String scoreStr = "  Your score: " + score;
        String BestScoreStr = "  Best Score: " + bestScore;
        Font myFont = new Font("SayMyName!", Font.BOLD, 11);
        g2.setColor(Color.BLACK);
        g2.setFont(myFont);
        Rectangle2D textBox1 = myFont.getStringBounds(scoreStr, g2.getFontRenderContext());
        Rectangle2D textBox2 = myFont.getStringBounds(scoreStr, g2.getFontRenderContext());
        g2.drawString(scoreStr, 0, (int) textBox1.getHeight());
        g2.drawString(BestScoreStr, 0, (int) textBox2.getHeight() + 15);
    }



    public void addCubeListToScreen(List<Cube> cubeList) {
        for (int i = 0; i < cubeList.size(); i++) {
            add(cubeList.get(i));
        }
    }

    public void paintNewCubeList(List<Cube> oldCubeList ) {
        for (int j = 0; j < oldCubeList.size(); j++) {
            remove(oldCubeList.get(j));
            validate();
            repaint();
        }
       matrixService.getCubeListFromMatrix(oldCubeList, matrix);
    }

    class TimerC implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (!turnOperationService.isLastCoord(cubeList,matrix)) {
                turnOperationService.turn(cubeList, "down",matrix);
                matrixService.getMatrixCoordFromList(cubeList,matrix);
                controlPanel.setCubeList(cubeList);
                controlPanel.setMatrix(matrix);

            } else {
//              delete olunmali line varsa, delete edir delete olunmush line sayini qaytarir
                int localScore = matrixService.deleteFullLine(matrix);
                if (localScore  > 0) {
                    paintNewCubeList(oldCubeList);
                }
                cubeList = shapeFactory.getCubeList(new Random().nextInt(9));
                if (utility.isGameOver(cubeList, matrix)){
                    if (score > bestScore){
                        try {
                            utility.setBestScore(score);
                        } catch (IOException ex) {
                            throw new RuntimeException("Couldn't set new bestscore");
                        }
                    }
                    timer.stop();
                }
                oldCubeList.addAll(cubeList);
                addCubeListToScreen(oldCubeList);
                controlPanel.setCubeList(cubeList);
                validate();
                repaint();
                score = score + utility.calculateScore(localScore);
            }
        }
    }
}
