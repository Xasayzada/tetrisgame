package com.tetris.game.service;

import com.tetris.game.domain.Cube;

import java.util.List;

public interface TurnOperationService {
    boolean isAbleToTurn(List<Cube> cubeList, String direction, boolean[][] matrix);
    void turn(List<Cube> cubeList, String direction, boolean[][] matrix);
    boolean isLastCoord(List<Cube> cubeList, boolean[][] matrix);
}
