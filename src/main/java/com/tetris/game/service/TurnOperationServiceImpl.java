package com.tetris.game.service;
import com.tetris.game.domain.Cube;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TurnOperationServiceImpl implements TurnOperationService {
    @Override
    public boolean isAbleToTurn(List<Cube> cubeList, String direction, boolean[][] matrix) {
        boolean canTurn = true;
        boolean[][] singleMatrixCoord = new boolean[12][16];

        for (int i = 0; i < cubeList.size(); i++) {
            singleMatrixCoord[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40] = true;
        }
//      Solda kub olub olmadiqini yoxlayir
        if (direction.equals("left")) {
            for (int i = 0; i < cubeList.size(); i++) {
                if (matrix[(cubeList.get(i).getX() / 40) - 1][cubeList.get(i).getY() / 40]) {
                    if (!singleMatrixCoord[(cubeList.get(i).getX() / 40) - 1][cubeList.get(i).getY() / 40]) {
                        canTurn = false;
                    }
                }
            }
        } else if (direction.equals("right")) {
            for (int i = 0; i < cubeList.size(); i++) {
                if (matrix[(cubeList.get(i).getX() / 40) + 1][cubeList.get(i).getY() / 40]) {
                    if (!singleMatrixCoord[(cubeList.get(i).getX() / 40) + 1][cubeList.get(i).getY() / 40]) {
                        canTurn = false;
                    }
                }
            }
        }
        return canTurn;
    }

    @Override
    public void turn(List<Cube> cubeList, String direction, boolean[][] matrix) {

            if (direction.equals("left")) {
                for (int i = 0; i < cubeList.size(); i++) {
                    int xCoord = cubeList.get(i).getX() - cubeList.get(i).getWidth();
                    int yCoord = cubeList.get(i).getY();
//          Kohne matrix koordinatlarini false edir
                    matrix[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40] = false;
                    cubeList.get(i).setBounds(xCoord, yCoord, 40, 40);
                    cubeList.get(i).setLine(cubeList.get(i).getLine() - 1);
                }
            } else if (direction.equals("right")) {
                for (int i = 0; i < cubeList.size(); i++) {
                    int xCoord = cubeList.get(i).getX() + cubeList.get(i).getWidth();
                    int yCoord = cubeList.get(i).getY();
                    matrix[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40] = false;
                    cubeList.get(i).setBounds(xCoord, yCoord, 40, 40);
                    cubeList.get(i).setLine(cubeList.get(i).getLine() + 1);
                }
            } else if (direction.equals("down")) {
                for (int i = 0; i < cubeList.size(); i++) {
                    int xCoord = cubeList.get(i).getX();
                    int yCoord = cubeList.get(i).getY() + cubeList.get(i).getHeight();
                    matrix[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40] = false;
                    cubeList.get(i).setBounds(xCoord, yCoord, 40, 40);
                }
            }
        }


    @Override
    public boolean isLastCoord(List<Cube> cubeList, boolean[][] matrix) {

        boolean isLastCoord = false;
        Map<Integer, Integer> cubeLastYCoorMap = new HashMap<>();

        for (int i = 0; i < cubeList.size(); i++) {
            int currentLine = cubeList.get(i).getLine();
            int curentYCoord = cubeList.get(i).getY();

            if (cubeLastYCoorMap.get(currentLine) != null) {
                if (cubeLastYCoorMap.get(currentLine) < curentYCoord + 40) {
                    cubeLastYCoorMap.put(currentLine, curentYCoord + 40);
                }
            } else {
                cubeLastYCoorMap.put(currentLine, curentYCoord + 40);
            }
        }

        for (Map.Entry<Integer, Integer> entry : cubeLastYCoorMap.entrySet()
        ) {
            if (matrix[entry.getKey()][(entry.getValue() / 40)]) {
                isLastCoord = true;
            }
        }
        return isLastCoord;
    }
}
