package com.tetris.game.service;

import com.tetris.game.domain.Cube;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class ControlPanel implements KeyListener {
    private boolean[][] matrix;
    private List<Cube> cubeList;
    private MatrixService matrixService;
    private TurnOperationService turnOperationService;

    public ControlPanel() {
        matrixService = new MatrixServiceImpl();
        turnOperationService = new TurnOperationServiceImpl();
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT && cubeList.get(0).getLine() != 0) {

            if (turnOperationService.isAbleToTurn(cubeList, "left", matrix)) {
                turnOperationService.turn(cubeList, "left", matrix);
//                  matrix coordinatlarini yenileyir
                matrixService.getMatrixCoordFromList(cubeList, matrix);
            }
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT && cubeList.get(1).getLine() != 11) {

            if (turnOperationService.isAbleToTurn(cubeList, "right", matrix)) {
                turnOperationService.turn(cubeList, "right",matrix);
                matrixService.getMatrixCoordFromList(cubeList,matrix);
            }
        }else if (e.getKeyCode() == KeyEvent.VK_DOWN){
            if (!turnOperationService.isLastCoord(cubeList,matrix)) {
                turnOperationService.turn(cubeList, "down",matrix);
                matrixService.getMatrixCoordFromList(cubeList,matrix);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    public void setMatrix(boolean[][] matrix) {
        this.matrix = matrix;
    }

    public void setCubeList(List<Cube> cubeList) {
        this.cubeList = cubeList;
    }
}