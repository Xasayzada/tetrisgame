package com.tetris.game.service;

import com.tetris.game.domain.Cube;

import java.util.List;

public class MatrixServiceImpl implements MatrixService {

    public void getMatrixCoordFromList(List<Cube> cubeList, boolean[][] matrix) {
        for (int i = 0; i < cubeList.size(); i++) {
            if (!matrix[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40]) {
                matrix[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40] = true;
            }
        }
    }

    public int deleteFullLine(boolean[][] matrix) {
        int deletedLine = 0;
        for (int i = 0; i < 15; i++) {
            boolean flag = false;
            for (int j = 0; j < 12; j++) {
                if (!matrix[j][i]) {
                    flag = true;
//                  flag true olsa hemin line-de bos kub var
//                  flag false olsa hemin line doludur
                }
            }
            if (flag == false) {
                deletedLine++;
                for (int j = i - 1; j >= 0; j--) {
                    for (int k = 0; k < 12; k++) {
                        matrix[k][j + 1] = matrix[k][j];
                    }
                }
            }
        }
        return deletedLine;
    }


    public void getCubeListFromMatrix(List<Cube> oldCubeList, boolean[][] matrix) {
        oldCubeList.removeAll(oldCubeList);
        for (int k = 0; k < 12; k++) {
            for (int j = 0; j < 15; j++) {
                if (matrix[k][j]) {
                    Cube newCube = new Cube();
                    newCube.setBounds(k * 40, j * 40, 40, 40);
                    oldCubeList.add(newCube);
                }
            }
        }
    }
}
