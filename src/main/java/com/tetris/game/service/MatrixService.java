package com.tetris.game.service;

import com.tetris.game.domain.Cube;

import java.util.List;

public interface MatrixService {
    void getMatrixCoordFromList(List<Cube> cubeList, boolean[][] matrix);
    int deleteFullLine(boolean[][] matrix);
    void getCubeListFromMatrix(List<Cube> oldCubeList, boolean[][] matrix);
}
