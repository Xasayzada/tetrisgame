package com.tetris.game.service;

import com.tetris.game.domain.Cube;

import java.io.*;
import java.util.List;

public class Utility {


    public int calculateScore(int localScore) {
        int score = 0;
        switch (localScore) {
            case 1:
                score = score + 10;
                break;
            case 2:
                score = score + 25;
                break;
            case 3:
                score = score + 50;
                break;
            case 4:
                score = score + 100;
                break;
            case 5:
                score = score + 200;
                break;
        }
        return score;
    }

    public boolean isGameOver(List<Cube> cubeList, boolean[][] matrix) {
        boolean isGameOver = false;
        for (int i = 0; i < cubeList.size(); i++) {
            if (matrix[cubeList.get(i).getX() / 40][cubeList.get(i).getY() / 40]) {
                System.out.println("GAME OVER");
                isGameOver = true;
                break;
            }
        }
        return isGameOver;
    }

    public int getBestScore() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("bestScore.txt"));
        int counter;
        String fileText = "";
        String currentLine = "";
        while ((currentLine = reader.readLine()) != null) {
            fileText = fileText + currentLine;
        }
        if (reader != null) {
            reader.close();
        }
        String[] bestScore = fileText.split("=");
        return Integer.parseInt(bestScore[1]);
    }

    public void setBestScore(int bestScore) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("bestScore.txt"));
        writer.write("bestscore=" + String.valueOf(bestScore));
        if (writer != null) {
            writer.close();
        }
    }
}
