package com.tetris.game;

import com.tetris.game.domain.Cube;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShapeFactory {

    private List<Cube> cubeList;
    private Random random;

    public ShapeFactory() {
        cubeList = new ArrayList<>();
        random = new Random();
    }

    public List<Cube> getCubeList(int random) {
        if (random == 6) {
            cubeList = get1CubeList();
        }
        if (random == 1) {
            cubeList = get2CubeList();
        }
        if (random == 2) {
            cubeList = get3CubeList();
        }
        if (random == 3) {
            cubeList = get4CubeList();
        }
        if (random == 4) {
            cubeList = get5CubeList();
        }
        if (random == 5) {
            cubeList = get6CubeList();
        }
        if (random == 0) {
            cubeList = get7CubeList();
        }
        if (random == 7) {
            cubeList = get8CubeList();
        }
        if (random == 8) {
            cubeList = get9CubeList();
        }
        return cubeList;
    }

    private List<Cube> get1CubeList() {
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        cube1.setBounds(240, 0, 40, 40);
        cube2.setBounds(240, 0, 40, 40);
        List<Cube> cubeListLocal = new ArrayList<>();
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube2);
        return cubeListLocal;
    }

    private List<Cube> get2CubeList() {
        List<Cube> cubeListLocal = new ArrayList<>();
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        cube1.setBounds(240, 0, 40, 40);
        cube1.setBounds(280, 0, 40, 40);
        cube2.setLine(7);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube2);
        return cubeListLocal;
    }

    private List<Cube> get5CubeList() {
        List<Cube> cubeListLocal = new ArrayList<>();
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        cube1.setBounds(240, 40, 40, 40);
        cube1.setBounds(240, 0, 40, 40);
        cube2.setLine(6);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube2);
        return cubeListLocal;
    }

    private List<Cube> get3CubeList() {
        List<Cube> cubeListLocal = new ArrayList<>();
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        Cube cube3 = new Cube();

        cube1.setBounds(200, 0, 40, 40);
        cube2.setBounds(240, 0, 40, 40);
        cube3.setBounds(280, 0, 40, 40);
        cube3.setLine(7);
        cube1.setLine(5);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube3);
        cubeListLocal.add(cube2);
        return cubeListLocal;
    }

    private List<Cube> get6CubeList() {
        List<Cube> cubeListLocal = new ArrayList<>();
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        Cube cube3 = new Cube();
//        cube1.setBounds(240, 0, 40, 40);
//        cube2.setBounds(240, 40, 40, 40);
//        cube3.setBounds(280, 0, 40, 40);
        cube1.setBounds(240, 0, 40, 40);
        cube2.setBounds(240, 40, 40, 40);
        cube3.setBounds(240, 80, 40, 40);
//        cube3.setLine(7);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube3);
        cubeListLocal.add(cube2);
        return cubeListLocal;
    }

    private List<Cube> get4CubeList() {
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        Cube cube3 = new Cube();
        Cube cube4 = new Cube();
        List<Cube> cubeListLocal = new ArrayList<>();
        cube1.setBounds(200, 0, 40, 40);
        cube2.setBounds(240, 0, 40, 40);
        cube3.setBounds(280, 0, 40, 40);
        cube4.setBounds(320, 0, 40, 40);
        cube1.setLine(5);
        cube3.setLine(7);
        cube4.setLine(8);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube4);
        cubeListLocal.add(cube2);
        cubeListLocal.add(cube3);
        return cubeListLocal;
    }

    private List<Cube> get7CubeList() {
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        Cube cube3 = new Cube();
        Cube cube4 = new Cube();
        List<Cube> cubeListLocal = new ArrayList<>();
        cube1.setBounds(240, 0, 40, 40);
        cube2.setBounds(280, 0, 40, 40);
        cube3.setBounds(240, 40, 40, 40);
        cube4.setBounds(280, 40, 40, 40);
        cube2.setLine(7);
        cube4.setLine(7);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube4);
        cubeListLocal.add(cube2);
        cubeListLocal.add(cube3);
        return cubeListLocal;
    }

    private List<Cube> get8CubeList() {
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        Cube cube3 = new Cube();
        Cube cube4 = new Cube();
        List<Cube> cubeListLocal = new ArrayList<>();
        cube2.setBounds(240, 0, 40, 40);
        cube4.setBounds(280, 0, 40, 40);
        cube3.setBounds(240, 40, 40, 40);
        cube1.setBounds(200, 40, 40, 40);
        cube1.setLine(5);
        cube4.setLine(7);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube4);
        cubeListLocal.add(cube2);
        cubeListLocal.add(cube3);
        return cubeListLocal;
    }

    private List<Cube> get9CubeList() {
        Cube cube1 = new Cube();
        Cube cube2 = new Cube();
        Cube cube3 = new Cube();
        Cube cube4 = new Cube();
        List<Cube> cubeListLocal = new ArrayList<>();
        cube2.setBounds(240, 0, 40, 40);
        cube4.setBounds(280, 40, 40, 40);
        cube3.setBounds(240, 40, 40, 40);
        cube1.setBounds(200, 40, 40, 40);
        cube1.setLine(5);
        cube4.setLine(7);
        cubeListLocal.add(cube1);
        cubeListLocal.add(cube4);
        cubeListLocal.add(cube2);
        cubeListLocal.add(cube3);
        return cubeListLocal;
    }
}
